---
sidebar_position: 1
<!-- LTeX: language=es -->
---
# Modo Puente/Bridge

## Requisitos

Mandar mensaje al WhatsApp de soporte técnico de Megacable y pedir:

  - Que te habiliten IP Pública.
  - Y que te pasen también las "credenciales de superusuario", diles que las quieres para ponerlo en modo bridge/modo puente
Una vez que tengas tu usuario (Mega_gpon) y tu contraseña podemos iniciar.
:::note
    Las credenciales expiran cada 24 h, si las pediste y dejaste pasar este tiempo tendrás que volver a pedirlas.
:::

### Iniciar sesión

1. Ve a la web ui del modem: https://gpon.net (o también https://192.168.1.1)

![Página de inicio de sesión](img/login.png)

2. Ingresa con tus credenciales de superusuario, el `Username` es siempre `Mega_gpon` y el `Password` te lo da soporte técnico y dura 24 h

### Crear interfaz bridge

1. Primero que nada ocupamos obtener por cuál ID de VLAN nos están conectando a internet

Ve a `Internet > WAN > WAN > WAN Connection > omci_ipv4_dhcp_1`
Y obtén el valor de `VLAN ID`

![Página de VLAN actual](img/current-vlan.png)

2. En la misma página de `WAN Connection`

![Agregar bridge](img/bridge-entry.png)

  1. Dale `Create New Item`
  2. Clic derecho a la página > Inspeccionar
  3. En la consola usa el botón de seleccionar elemento
  4. Selecciona el cuadro de selección de `Type`
  5. Duplica la opción con `value=route` y la nueva opción duplicada cámbiale el valor a `value=bridge`, `title=bridge` y cambia el nombre de `>Routing<` a `>Bridge<`
  6. Dale nombre a la nueva interfaz en el campo `Connection Name` a `bridge1` (por ejemplo) y cambia el `Type` a `Bridge`
  
  ![Configurar VLAN](img/set-vlan.png)

  7. Cambia `VLAN` a `On`
  8. Setea `VLAN ID` con el valor que sacamos del primer paso de esta sección (en este caso `558`)
  9. Selecciona Apply
### Configurar puerto LAN para bridge

![Asignar puerto bridge](img/port-assign.png)

### Configurar servidor DHCPv4 para excluir la operación en la interfaz bridge

![Deshabilitar DHCPv4 en puerto bridge](img/dhcpv4-disable.png)

### Configurar servidor DHCPv6 para excluir la operación en la interfaz bridge

![Deshabilitar DHCPv6 en puerto bridge](img/dhcpv6-disable.png)
